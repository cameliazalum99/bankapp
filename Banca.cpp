#pragma once
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include "Cont.cpp"
#include "Exceptions.cpp"
using namespace std;
class Banca {
	vector<Cont> conturi;
public :
	Banca(string file)
	{
		ifstream fin(file);
		string cont, pin;
		int bani;
		while (fin >> cont >> pin >> bani)
		{
			Cont *a = new Cont(cont, pin, bani);
			conturi.push_back(*a);
		}
	}
	vector<string> splitBySpace(string op) {
		vector<string> result;
		string word = "";
		char separator = ' ';
		for (int i = 0; i < op.size(); i++) {
			if (op[i] == separator) {
				result.push_back(word);
				word = "";
			}
			else {
				word += op[i];
			}
		}
		result.push_back(word);
		return result;
		
	}
	void executaOperatiune(string const& operatiune) {
		vector<string> words = splitBySpace(operatiune);
		if (words[0] == "DEPOZIT")
		{
			if (stoi(words[3]) < 10)
			{
				string out = "Suma " + words[3] + " este prea mica pentru a realiza " + operatiune;
				throw Exceptions(out, 10, 10);
			}
			else {
				bool found = false;
				for (int i = 0; i < conturi.size(); i++)
				{
					if (conturi[i].cont == words[1])
					{
						found = true;
						if (conturi[i].pin == words[2])
						{
							int suma = stoi(words[3]);
							conturi[i].suma += suma;
						}
						else {
							string out = "Pinul pentru " + words[1] + " este gresit ";
							throw Exceptions(out, 10, 10);
						}
					}
				}
				if (!found) {
					string out = "Contul " + words[1] + " nu exista.";
					throw Exceptions(out, 10, 10);
				}
			}
		}
		else if (words[0] == "RETRAGERE") {
			if (stoi(words[3]) < 15) {
				string out = "Suma " + words[3] + " este prea mica pentru a realiza " + operatiune;
				throw Exceptions(out, 10, 10);
			}
			else {
				bool found = false;
				for (int i = 0; i < conturi.size(); i++)
				{
					if (conturi[i].cont == words[1])
					{
						found = true;
						if (conturi[i].pin == words[2])
						{
							int suma = stoi(words[3]);
							if (suma < conturi[i].suma) {
								conturi[i].suma -= suma;
							}
							else {
								string out = "Contul " + words[1] + " nu poate retrage " + words[3];
								throw Exceptions(out, 10, 10);
							}
						}
						else {
							string out = "Pinul pentru " + words[1] + " este gresit ";
							throw Exceptions(out, 10, 10);
						}
					}
				}
				if (!found) {
					string out = "Contul " + words[1] + " nu exista.";
					throw Exceptions(out, 10, 10);
				}
			}

		} else if (words[0] == "TRANSFER") {
			if (stoi(words[4]) < 50) {
				string out = "Suma " + words[3] + " este prea mica pentru a realiza " + operatiune;
				throw Exceptions(out, 10, 10);
			}
			else {
				int firstIndex;
				int secondIndex;
				bool found1 = false, found2 = false;
				for (int i = 0; i < conturi.size(); i++)
				{
					if (conturi[i].cont == words[1])
					{
						found1 = true;
						if (conturi[i].pin == words[2])
						{
							firstIndex = i;
						}
						else {
							string out = "Pinul pentru " + words[1] + " este gresit ";
							throw Exceptions(out, 10, 10);
						}
					}
				}
				for (int i = 0; i < conturi.size(); i++)
				{
					if (conturi[i].cont == words[3])
					{
						found2 = true;
						secondIndex = i;
					}
				}
				if (!found1) {
					string out = "Contul " + words[1] + " nu exista.";
					throw Exceptions(out, 10, 10);
				}
				if (!found2) {
					string out = "Contul " + words[3] + " nu exista.";
					throw Exceptions(out, 10, 10);
				}
				if (found1 && found2) {
					int suma = stoi(words[4]);
					if (conturi[firstIndex].suma > suma)
					{
						conturi[firstIndex].suma -= suma;
						conturi[secondIndex].suma += suma;

					}
					else {
						string out = "Contul " + words[1] + " nu poate transfera " + words[4];
						throw Exceptions(out, 10, 10);
					}
				}
			}
			
		}
	}
};