// BankApp.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "Banca.cpp"
using namespace std;

int main()
{
	Banca *app = new Banca("start.txt");
	while (true)
	{
		cout << "tip de OPERATIUNE : \n1. Depozit\n2.Retragere\n3.Transfer";
		int tip;
		cin >> tip;
		string cont;
		string op = "";
		if (tip == 1)
		{
			op += "DEPOZIT ";
			cout << "contul : ";
			cin >> cont;
			op += cont + " ";
			string pin;
			cout << "\npin : ";
			cin >> pin;
			op += pin + " ";
			int suma;
			cout << "\nsuma: ";
			cin >> suma;
			op += to_string(suma);
			try {
				app->executaOperatiune(op);
				cout << " done";
			}
			catch (Exceptions e)
			{
				cout << e.what();
			}
		}
		if (tip == 2)
		{
			op += "RETRAGERE ";
			cout << "contul : ";
			cin >> cont;
			op += cont + " ";
			string pin;
			cout << "\npin : ";
			cin >> pin;
			op += pin + " ";
			int suma;
			cout << "\nsuma: ";
			cin >> suma;
			op += to_string(suma);
			try {
				app->executaOperatiune(op);
				cout << " done";
			}
			catch (Exceptions e)
			{
				cout << e.what();
			}
		}
		if (tip == 3)
		{
			op += "TRANSFER ";
			cout << "contul : ";
			cin >> cont;
			op += cont + " ";
			string pin;
			cout << "\npin : ";
			cin >> pin;
			op += pin + " ";
			string destinatie;
			cout << "\ndestinaie : ";
			cin >> destinatie;
			op += destinatie + " ";
			int suma;
			cout << "\nsuma: ";
			cin >> suma;
			op += to_string(suma);
			try {
				app->executaOperatiune(op);
				cout << " done";
			}
			catch (Exceptions e)
			{
				cout << e.what();
			}
		}
	}
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
