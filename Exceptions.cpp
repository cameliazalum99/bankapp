#include <exception>
#include <string>
using namespace std;
class Exceptions : public exception
{
protected:

    int error_number;               ///< Error number
    int error_offset;               ///< Error offset
    string error_message;      ///< Error message
public:

    explicit
        Exceptions(const string& msg, int err_num, int err_off) :
        error_number(err_num),
        error_offset(err_off),
        error_message(msg)
    {}
    virtual ~Exceptions() throw () {}
        virtual const char* what() const throw () {
            return error_message.c_str();
        }
        virtual int getErrorNumber() const throw() {
            return error_number;
        }
        virtual int getErrorOffset() const throw() {
            return error_offset;
        }

};